# Fucking dropbox project

This project is used, with a dropbox api in python 

### Installation
You need at least `Python 3.8` and `pip` installed to run this CLI tool.
1. Install it using `pip`
    ```shell
    pip install dropbox_api_team_3
    ```
2. We can't help
    ```
   :)
### Available commands

1. `upload-file` 
to upload a local file to dropbox 
2. `transfer_file` 
to upload a file from the internet to dropbox 
3. `rename_file` to rename the file to dropbox 
4. `delete_file` to delete file on dropbox 
5. `download_file` to download the file to dropbox 

### Optional parameters 
1. `--destination_dir` to determine which directory ON the dropbox (in the case of `download_file` which directory LOCAL) this file will be loaded 
2. `--api_token` for the API token on which the work will take place. If you do not pass a value, then it will be searched in the DROPBOX_API_TOKEN environment variable (.env files are also supported ), if not there, an error is thrown. 

### There is no more information

```
:)
```